FROM quay.io/pypa/manylinux2010_x86_64
ENV RUSTUP_HOME=/usr/local CARGO_HOME=/usr/local
RUN curl --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y --no-modify-path --default-toolchain nightly
RUN for PIP in /opt/python/*/bin/pip; do $PIP install --no-cache-dir setuptools-rust; done
